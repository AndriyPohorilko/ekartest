//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

extension UIColor {
    static let lightBlue = UIColor(
        red: 226 / 255, green: 246 / 255, blue: 254 / 255, alpha: 1)
}

extension Color {
    init(red: Double, green: Double, blue: Double) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255, opacity: 1)
    }
}

extension Color {
    static let customBlue = Color(red: 116, green: 198, blue: 245)
    static let lightBlue = Color(.lightBlue)
    static let customPink = Color(red: 236, green: 81, blue: 113)
}
