//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine
import CoreLocation

class DefaultCurrentLocationManager: NSObject, ICurrentLocationManager, CLLocationManagerDelegate {
    
    private let locationManager = CLLocationManager()
    
    var locationStatus: AnyPublisher<CLAuthorizationStatus, Never> {
        locationStatusSubject.eraseToAnyPublisher()
    }
    private let locationStatusSubject = PassthroughSubject<CLAuthorizationStatus, Never>()
    
    var currentLocation: PassthroughSubject<CLLocation, Never> {
        lastLocationSubject
    }
    private let lastLocationSubject = PassthroughSubject<CLLocation, Never>()
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationStatusSubject.send(status)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        lastLocationSubject.send(location)
    }
}
