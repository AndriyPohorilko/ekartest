//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine
import CoreLocation

protocol ICurrentLocationManager: AnyObject {
    var locationStatus: AnyPublisher<CLAuthorizationStatus, Never> { get }
    var currentLocation: PassthroughSubject<CLLocation, Never> { get }
}
