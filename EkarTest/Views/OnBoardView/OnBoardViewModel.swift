//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation

class OnBoardViewModel: ObservableObject {
    @Published var showSuccessMessage = false
    @Published var isValidToProceed = false
    
    @Published var commentText: String = ""
    
    init() {
        $commentText
            .map { !$0.isEmpty }
            .assign(to: &$isValidToProceed)
    }
}
