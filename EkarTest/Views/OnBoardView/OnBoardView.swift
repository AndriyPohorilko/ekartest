//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct OnBoardView: View {
    @ObservedObject var viewModel: OnBoardViewModel
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack {
            topView
            ScrollView {
                VStack(spacing: 10) {
                    Text("Please upload clear photos of the vehicle to avoid liability of any damages caused before starting your reservation")
                    CapturePhotosView()
                    Spacer()
                }
            }
            commentView
            ProceedButton(title: "Submit", isEnabled: viewModel.isValidToProceed, action: {
                viewModel.showSuccessMessage = true
            })
        }
        .zStacked {
            if viewModel.showSuccessMessage {
                successMessage
            }
        }
    }
    
    private var successMessage: some View {
        ToastView(show: $viewModel.showSuccessMessage, text: "Thank you for choosing ekar")
    }
    
    private var topView: some View {
        ZStack {
            HStack {
                CloseButton(action: {
                    presentationMode.wrappedValue.dismiss()
                })
                Spacer()
            }
            .padding(.leading)
            
            Logo()
        }
    }
    
    private var commentView: some View {
        VStack(alignment: .leading) {
            SmallTitleLabel(text: "Leave a comment:")
            
            VStack {
                TextView(text: $viewModel.commentText)
                    .frame(height: 30)
                Divider()
            }
            
        }
        .padding()
    }
}

private struct CapturePhotosView: View {
    private let rows = [
        GridItem(.fixed(120)),
        GridItem(.fixed(120))
    ]
    
    private var icons: [String] {
        [
            "front/left",
            "front/right",
            "back/left",
            "back/right"
        ]
    }
    
    @State private var showCaptureImage = false
    @State private var imageToCapture: String?
    
    var body: some View {
        LazyHGrid(rows: rows, spacing: 30) {
            ForEach(0 ..< icons.count, id: \.self) { index in
                let item = icons[index]
                getItemView(item)
            }
        }
        .padding(35)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.lightGray), lineWidth: 1)
        )
        .zStacked {
            if showCaptureImage,
               let imageToCapture = imageToCapture{
                ToastView(show: $showCaptureImage, text: "Start to capture \"\(imageToCapture)\"")
            }
        }
    }
    
    private func getItemView(_ item: String) -> some View {
        Button(action: {
            imageToCapture = item
            showCaptureImage = true
        }, label: {
            VStack {
                Image(item.replacingOccurrences(of: "/", with: ""))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                
                Text(item.uppercased())
                    .foregroundColor(Color(.gray))
            }
        })
    }
}

struct OnBoardView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardView(viewModel: OnBoardViewModel())
    }
}
