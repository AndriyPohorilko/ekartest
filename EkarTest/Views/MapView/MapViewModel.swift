//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine
import MapKit

class MapViewModel: ObservableObject {
    @Published var vehicles: [VehicleOnMap] = []
    @Published var region: MKCoordinateRegion = MKCoordinateRegion()
    
    private let vehiclesProvider: IVehiclesOnMapDataProvider
    private let locationManager: ICurrentLocationManager
    private let coordinator: IMapViewCoordinator
    
    private var subscriptions = Set<AnyCancellable>()
    
    private let defaultCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
    
    init(
        vehiclesProvider: IVehiclesOnMapDataProvider,
        locationManager: ICurrentLocationManager,
        coordinator: IMapViewCoordinator
    ) {
        self.vehiclesProvider = vehiclesProvider
        self.locationManager = locationManager
        self.coordinator = coordinator
        
        locationManager.locationStatus
            .receive(on: RunLoop.main)
            .sink { [weak self] status in
                guard let self = self else { return }
                
                print(self.readLocationManagerStatus(status))
            }
            .store(in: &subscriptions)
        
        locationManager.currentLocation
            .first()
            .receive(on: RunLoop.main)
            .handleEvents(receiveOutput: { [weak self] location in
                guard let self = self else { return }
                
                self.getCars(location: location)
            })
            .map { [weak self] location in
                guard let self = self else { return MKCoordinateRegion() }
                
                return self.createCoordinateRegion(location: location)
            }
            .assign(to: &$region)
    }
    
    private func readLocationManagerStatus(_ status: CLAuthorizationStatus) -> String {
        switch status {
        case .notDetermined: return "notDetermined"
        case .authorizedWhenInUse: return "authorizedWhenInUse"
        case .authorizedAlways: return "authorizedAlways"
        case .restricted: return "restricted"
        case .denied: return "denied"
        default: return "unknown"
        }
    }
    
    private func createCoordinateRegion(location: CLLocation) -> MKCoordinateRegion {
        MKCoordinateRegion(center: location.coordinate, span: defaultCoordinateSpan)
    }
    
    private func getCars(location: CLLocation) {
        vehiclesProvider.getVehiclesAroundLocation(location: location.coordinate)
            .receive(on: RunLoop.main)
            .catch { error -> Just<[VehicleOnMap]> in
                // TODO: - show error to the users
                print(#function, "error", error.localizedDescription)
                return Just([])
            }
            .assign(to: &$vehicles)
    }
    
    func onCarSelect(_ car: VehicleOnMap) {
        coordinator.onVehicleSelect(car)
    }
}
