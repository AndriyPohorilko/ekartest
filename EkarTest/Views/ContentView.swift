//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct ContentView: View {
    @ObservedObject var appCoordinator: AppCoordinator
    @ObservedObject var mapViewModel: MapViewModel
    
    @State private var showVehicleView = false
    @State private var selectedVinCode: VinCode?
    
    @State private var showOnBoardView = false
    
    var body: some View {
        NavigationView {
            MapView(
                viewModel: mapViewModel
            )
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Logo()
                }
            }
            .zStacked {
                if let selectedVinCode = selectedVinCode {
                    NavigationLink(
                        destination: createVehicleView(vinCode: selectedVinCode),
                        isActive: $showVehicleView,
                        label: { })
                }
            }
            .fullScreenCover(isPresented: $showOnBoardView, content: {
                OnBoardView(viewModel: OnBoardViewModel())
            })
        }
        .onAppear {
            ThemeManager.shared.handleTheme(darkMode: false)
        }
        .onReceive(appCoordinator.selectedVehicle, perform: { selectedVinCode in
            self.selectedVinCode = selectedVinCode
            showVehicleView = true
        })
        .onReceive(appCoordinator.showOnBoardView, perform: { _ in
            showOnBoardView = true
        })
    }
    
    private func createVehicleView(vinCode: VinCode) -> some View {
        VehicleView(
            viewModel: VehicleViewModel(
                vinCode: vinCode,
                vehicleProvider: DefaultVehicleInfoProvider(),
                coordinator: appCoordinator
            )
        )
    }
}
