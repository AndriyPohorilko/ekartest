//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine

protocol IMapViewCoordinator: AnyObject {
    func onVehicleSelect(_ vehicle: VehicleOnMap)
}

protocol IVehicleDetailsCoordinator {
    func onProceedTap()
}

class AppCoordinator: IMapViewCoordinator, IVehicleDetailsCoordinator, ObservableObject {
    
    // MARK: - IMapViewCoordinator
    
    var selectedVehicle: AnyPublisher<VinCode, Never> { selectedVehicleSubject.eraseToAnyPublisher() }
    private let selectedVehicleSubject = PassthroughSubject<VinCode, Never>()
    
    var showVehicleView: AnyPublisher<Void, Never> { showVehicleDetailsSubject.eraseToAnyPublisher() }
    private let showVehicleDetailsSubject = PassthroughSubject<Void, Never>()
    
    func onVehicleSelect(_ vehicle: VehicleOnMap) {
        selectedVehicleSubject.send(vehicle.vin)
    }
    
    // MARK: - IVehicleDetailsCoordinator
    
    var showOnBoardView: AnyPublisher<Void, Never> { showOnBoardViewSubject.eraseToAnyPublisher() }
    private let showOnBoardViewSubject = PassthroughSubject<Void, Never>()
    
    func onProceedTap() {
        showOnBoardViewSubject.send()
    }
}
