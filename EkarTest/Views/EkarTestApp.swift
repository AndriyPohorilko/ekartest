//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

@main
struct EkarTestApp: App {
    @StateObject var appCoordinator = AppCoordinator()
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView(
                appCoordinator: appCoordinator,
                mapViewModel: MapViewModel(
                    vehiclesProvider: DefaultVehiclesOnMapDataProvider(),
                    locationManager: DefaultCurrentLocationManager(),
                    coordinator: appCoordinator
                )
            )
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
}

class ThemeManager {
    static let shared = ThemeManager()

    private init () {}

    func handleTheme(darkMode: Bool) {
        UIApplication.shared.windows.first?.overrideUserInterfaceStyle = darkMode ? .dark : .light
    }
}
