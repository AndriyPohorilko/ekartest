//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct ZStackedView<ZStackedView: View>: ViewModifier {
    let alignment: Alignment
    let view: (() -> ZStackedView)
    
    init(alignment: Alignment = .center, @ViewBuilder view: @escaping () -> ZStackedView) {
        self.alignment = alignment
        self.view = view
    }
    
    func body(content: Content) -> some View {
        ZStack(alignment: alignment) {
            content
            view()
                .zIndex(1)
        }
    }
}

extension View {
    func zStacked<T: View>(alignment: Alignment = .center, @ViewBuilder view: @escaping () -> T) -> some View {
        self.modifier(ZStackedView(alignment: alignment, view: view))
    }
}

