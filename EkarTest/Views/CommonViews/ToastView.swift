//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct ToastView: View {
    @Binding var show: Bool
    let text: String
    
    var body: some View {
        ZStack {
            Color(.darkGray).opacity(0.8)
                .edgesIgnoringSafeArea(.all)
                .transition(.opacity)
                .onTapGesture {
                    withAnimation {
                        show = false
                    }
                }
            
            ZStack {
                RoundedRectangle(cornerRadius: 4, style: .continuous)
                    .fill(Color.lightBlue)
                Text(text)
                    .multilineTextAlignment(.center)
                    .padding(40)
                    
            }
            .fixedSize(horizontal: false, vertical: true)
            .transition(.scale.animation(.easeIn(duration: 0.2)))
            
            .zStacked(alignment: .topTrailing) {
                CloseButton(action: {
                    show = false
                })
                .padding(5)
            }
            .padding()
        }
    }
}

struct ToastView_Previews: PreviewProvider {
    static var previews: some View {
        ToastView(show: .constant(true), text: "Some test toast message")
    }
}
