//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

extension View {
    func loadingView(isLoading: Bool) -> some View {
        Group {
            if isLoading {
                self.zStacked {
                    LoadingIndicator()
                }
            } else {
                self
            }
        }
    }
}

struct LoadingIndicator: View {
    
    var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle(tint: Color.customBlue))
            .scaleEffect(2)
    }
}

struct LoadingIndicator_Previews: PreviewProvider {
    static var previews: some View {
        LoadingIndicator()
    }
}
