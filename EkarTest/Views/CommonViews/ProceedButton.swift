//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct ProceedButton: View {
    let title: String
    var isEnabled: Bool = true
    let action: () -> Void
    
    var body: some View {
        Button(action: action, label: {
            Text("Proceed with your selection")
                .bold()
                .padding()
                .frame(maxWidth: .infinity)
                .background(Color.customBlue)
                .foregroundColor(.white)
                .cornerRadius(6)
                .opacity(isEnabled ? 1 : 0.6)
        })
        .disabled(!isEnabled)
    }
}

struct ProceedButton_Previews: PreviewProvider {
    static var previews: some View {
        ProceedButton(title: "Proceed", action: {  })
    }
}
