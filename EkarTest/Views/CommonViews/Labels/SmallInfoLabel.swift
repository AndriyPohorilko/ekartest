//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct SmallInfoLabel: View {
    let text: String
    
    var body: some View {
        Text(text)
            .font(.caption)
    }
}

struct SmallInfoLabel_Previews: PreviewProvider {
    static var previews: some View {
        SmallInfoLabel(text: "Test")
    }
}
