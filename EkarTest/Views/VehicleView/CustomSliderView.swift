//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct CustomSliderView: View {
    @State private var value: Float = 0
    
    let periods: [Int]
    let width: CGFloat
    
    var body: some View {
        VStack {
            slider(width: width)
            labelsView(maxWidth: width)
        }
        .frame(width: width)
        .onAppear {
            if !periods.isEmpty {
                value = calculatePercentValues(periods: periods, maxWidth: width)[0]
            }
        }
    }
    
    private func slider(width: CGFloat) -> some View {
        ZStack(alignment: Alignment(horizontal: .leading, vertical: .center), content: {
            wholeLine
            maximumTrack(maxWidth: width)
            thumbView(maxWidth: width)
        })
    }
    
    private func labelsView(maxWidth: CGFloat) -> some View {
        HStack(spacing: 0) {
            ForEach(0 ..< periods.count, id: \.self) { index in
                Text("\(periods[index])")
                    .frame(maxWidth: .infinity)
                    .onTapGesture {
                        let values = calculatePercentValues(periods: periods, maxWidth: maxWidth)
                        withAnimation(.easeInOut) {
                            value = values[index]
                        }
                    }
            }
        }
        .padding(.horizontal, 0)
    }
    
    private func calculatePercentValues(periods: [Int], maxWidth: CGFloat) -> [Float] {
        var values = [Float]()
        for i in 0..<periods.count {
            let oneSectionWidth = Float(maxWidth) / Float(periods.count)
            let percent = oneSectionWidth * Float(i) + (oneSectionWidth / 3)
            values.append(percent)
        }
        
        return values
    }
    
    private func getClosestValue(values: [Float], x: Float) -> Float? {
        return values
            .enumerated()
            .min(by: { abs($0.1 - x) < abs($1.1 - x) })?
            .element
    }
    
    private var wholeLine: some View {
        Capsule()
            .fill(Color.customBlue)
            .frame(height: 10)
    }
    
    private func thumbView(maxWidth: CGFloat) -> some View {
        let size: CGFloat = 30
        
        return Circle()
            .strokeBorder(Color.customBlue,lineWidth: 7)
            .background(Circle().foregroundColor(Color.white))
            .frame(width: size, height: size)
            .offset(x: CGFloat(self.value))
            .gesture(
                DragGesture()
                    .onChanged { value in
                        if value.location.x > 0 &&
                            value.location.x < CGFloat(maxWidth - size) {
                            withAnimation(.easeInOut) {
                                self.value = Float(value.location.x)
                            }
                        }
                    }
                    .onEnded { value in
                        let values = calculatePercentValues(periods: periods, maxWidth: maxWidth)
                        if let closest = getClosestValue(values: values, x: Float(value.location.x)) {
                            withAnimation(.easeInOut) {
                                self.value = closest
                            }
                        }
                    }
            )
    }
    
    private func maximumTrack(maxWidth: CGFloat) -> some View {
        Capsule()
            .strokeBorder(Color.customBlue,lineWidth: 2)
            .background(Capsule().foregroundColor(Color.white))
            .offset(x: CGFloat(self.value))
            .frame(width: maxWidth - CGFloat(self.value), height: 10)
    }
}

struct SliderView2_Previews: PreviewProvider {
    static var previews: some View {
        CustomSliderView(periods: [1, 3, 6, 9], width: UIScreen.main.bounds.width - 40)
    }
}
