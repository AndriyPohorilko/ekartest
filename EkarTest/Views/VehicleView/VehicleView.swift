//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import SwiftUI

struct VehicleView: View {
    @ObservedObject var viewModel: VehicleViewModel
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                ScrollView(.vertical, showsIndicators: false) {
                    MainInfoView(viewModel: viewModel, geometry: geometry)
                    AboutVehicleView(vehicle: viewModel.vehicle)
                }
                ProceedWithSelectionView(viewModel: viewModel)
            }
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                Logo()
            }
        }
        .zStacked {
            if viewModel.showHowContractsWorkView {
                howContractsWorkView
            }
            
            if viewModel.showSelectedColorView,
               let selectedColor = viewModel.selectedColor {
                selectedColorView(color: selectedColor)
            }
        }
        .loadingView(isLoading: viewModel.isLoading)
    }
    
    private var howContractsWorkView: some View {
        ToastView(show: $viewModel.showHowContractsWorkView, text: "How contracts work pressed.")
    }
    
    private func selectedColorView(color: String) -> some View {
        ToastView(show: $viewModel.showSelectedColorView, text: "\"\(color)\" selected")
    }
}

struct VehicleView_Previews: PreviewProvider {
    static let viewModel = VehicleViewModel(
        vinCode: VinCode(code: ""),
        vehicleProvider: TestVehicleDataProvider(),
        coordinator: AppCoordinator()
    )
    static var previews: some View {
        VehicleView(viewModel: viewModel)
    }
}

private struct MainInfoView: View {
    @ObservedObject var viewModel: VehicleViewModel
    let geometry: GeometryProxy
    
    init(viewModel: VehicleViewModel, geometry: GeometryProxy) {
        self.viewModel = viewModel
        self.geometry = geometry
        
        configurePageControl()
       }
    
    private func configurePageControl() {
        let iconConfig = UIImage.SymbolConfiguration(pointSize: 10, weight: .heavy, scale: .small)
        
        UIPageControl.appearance().preferredIndicatorImage = UIImage(systemName: "circle", withConfiguration: iconConfig)
        UIPageControl.appearance().currentPageIndicatorTintColor = UIColor.lightBlue
        UIPageControl.appearance().pageIndicatorTintColor = UIColor.lightBlue.withAlphaComponent(0.2)
    }
    
    var body: some View {
        let padding: CGFloat = 10
        ZStack {
            Color.lightBlue
            
            VStack {
                carImageTabView
                VStack(spacing: 20) {
                    yearAndColorsView
                    priceAndContractView
                    tenureView
                    sliderView(width: geometry.frame(in: .global).width - padding * 2)
                    bookingFeeView
                }
            }
            .padding(padding)
        }
    }
    
    private var bookingFeeView: some View {
        HStack {
            VStack(alignment: .leading) {
                SmallTitleLabel(text: "Booking Fee".uppercased())
                HStack(alignment: .bottom) {
                    SmallInfoLabel(text: "AED")
                    LargeInfoLabel(text: "120")
                }
            }
            Spacer()
            
            Button(action: {
                viewModel.onHowContractsWorkPressed()
            }, label: {
                SmallTitleLabel(text: "how contracts work?")
                    .foregroundColor(Color.customBlue)
                    .padding(.horizontal)
                    .padding(.vertical, 10)
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 6)
                            .stroke(Color.customBlue, lineWidth: 1)
                    )
                    
            })
        }
    }
    
    private func sliderView(width: CGFloat) -> some View {
        CustomSliderView(periods: [1, 3, 6, 9], width: width)
    }
    
    private var tenureView: some View {
        VStack {
            HStack {
                VerySmallLabel(text: "tenure")
                Spacer()
            }
            HStack {
                SmallTitleLabel(text: "1 to 9 month")
                Spacer()
                RoundedInfoView(
                    text: "savings of AED 1500".uppercased(),
                    foregroundColor: .white,
                    backgroundColor: Color.customPink
                )
            }
        }
    }
    
    private var priceAndContractView: some View {
        HStack {
            VStack(alignment: .leading) {
                SmallTitleLabel(text: "Base Price")
                HStack(alignment: .bottom) {
                    LargeInfoLabel(text: viewModel.vehicle.price.value)
                    SmallInfoLabel(text: viewModel.vehicle.price.currency)
                }
            }
            
            Spacer()
            
            VStack(alignment: .trailing) {
                SmallTitleLabel(text: "Contract Lenght")
                // since there is no info in API, here is some placeholder data
                HStack(alignment: .bottom) {
                    LargeInfoLabel(text: "3")
                    SmallInfoLabel(text: "Month")
                }
            }
        }
    }
    
    private var yearAndColorsView: some View {
        HStack {
            yearLabel
            Spacer()
            availableColorsView
        }
    }
    
    private var carImageTabView: some View {
        TabView {
            carImage("Toyota Prius V 1")
            carImage("Toyota Prius V 2")
        }
        .foregroundColor(.blue)
        .tabViewStyle(PageTabViewStyle())
        .frame(height: 190, alignment: .top)
        .padding(.horizontal, 50)
    }
    
    private func carImage(_ name: String) -> some View {
        Image(name)
            .resizable()
            .aspectRatio(contentMode: .fit)
    }
    
    private var yearLabel: some View {
        VerySmallLabel(text: "Year - \(viewModel.vehicle.year)")
    }
    
    private let colorsCountLimit = 4
    
    private var availableColorsView: some View {
        HStack {
            VerySmallLabel(text: "Available colors")
            
            if viewModel.vehicle.colors.count > colorsCountLimit {
                HStack(spacing: 5) {
                    ForEach(0 ..< viewModel.vehicle.colors[0..<colorsCountLimit].count, id: \.self) { index in
                        getColorView(viewModel.vehicle.colors[index])
                    }
                }
            }
        }
    }
    
    private func getColorView(_ color: String) -> some View {
        Button(action: {
            viewModel.onColorSelect(color)
        }, label: {
            Image(systemName: "circle.fill")
                .font(.caption)
                .foregroundColor(getColor(color))
        })
        
    }
    
    private func getColor(_ name: String) -> Color? {
        switch name {
        case "Barcelona Red Met":
            return Color.red
        case "Black":
            return Color.black
        case "Blizzard Pearl":
            return Color.purple
        case "Blue Ribbon Metallic":
            return Color.blue
        default:
            return nil
        }
    }
}

private struct AboutVehicleView: View {
    let vehicle: Vehicle
    
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("About the vehicle")
                    .bold()
                    .font(.title)

                HStack(spacing: 5) {
                    ForEach(0 ..< vehicle.infoItems.count, id: \.self) { index in
                        getView(vehicle.infoItems[index])
                    }
                }
            }
            KeyFeaturesView(vehicle: vehicle)
        }
        .padding()
    }
    
    private func getView(_ item: VehicleInfoItem) -> some View {
        let iconHeight: CGFloat = 50
        
        return ZStack {
            Color.lightBlue
                .cornerRadius(10)
            VStack(spacing: 5) {
                Image(systemName: item.type.icon)
                    .font(.title)
                    .frame(height: iconHeight)
                    .padding(.horizontal, 20)

                VerySmallLabel(text: item.value)
                    .lineLimit(1)
            }
            .padding(.vertical, 10)
        }
    }
}

private struct KeyFeaturesView: View {
    let vehicle: Vehicle

    let columns = [
        GridItem(.adaptive(minimum: 100))
    ]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Key features")
            
            LazyVGrid(columns: columns) {
                ForEach(0 ..< filteredItems.count, id: \.self) { index in
                    let item = filteredItems[index]
                    getItemView(item)
                }
            }
        }
    }
    
    private var filteredItems: [EquipmentItem] {
        let filteredItems = vehicle.equipmentItems.filter {
            $0.value != "N/A"
        }
        
        if filteredItems.count > 5 {
            return Array(filteredItems[0...5])
        }
        
        return filteredItems
    }
    
    private func getItemView(_ item: EquipmentItem) -> some View {
        RoundedInfoView(
            text: item.name,
            foregroundColor: Color(.label),
            backgroundColor: Color.lightBlue
        )
    }
}

private struct RoundedInfoView: View {
    let text: String
    let foregroundColor: Color
    let backgroundColor: Color
    
    var body: some View {
        Text(text)
            .multilineTextAlignment(.center)
            .font(.caption2)
            .padding(.horizontal, 20)
            .padding(.vertical, 5)
            .foregroundColor(foregroundColor)
            .background(backgroundColor)
            .clipShape(Capsule())
    }
}

private struct ProceedWithSelectionView: View {
    @ObservedObject var viewModel: VehicleViewModel
    
    var body: some View {
        VStack(spacing: 20) {
            vehicleView
            proceedWithSelectionButton
        }
        .padding(30)
        .overlay(
            RoundedRectangle(cornerRadius: 3)
                .stroke(Color.gray, lineWidth: 1)
        )
    }
    
    private var vehicleView: some View {
        HStack(spacing: 10) {
            logo
            nameView
            Spacer()
        }
    }
    
    private var logo: some View {
        let iconSize: CGFloat = 40
        
        return Image("toyota_logo")
            .resizable()
            .frame(width: iconSize, height: iconSize)
    }
    
    private var nameView: some View {
        VStack(alignment: .leading, spacing: 5) {
            HStack {
                Text(viewModel.vehicle.make)
                    .bold()
                Text(viewModel.vehicle.model)
            }
            Text(viewModel.vehicle.style)
        }
    }
    
    private var proceedWithSelectionButton: some View {
        ProceedButton(title: "Proceed with your selection", action: {
            viewModel.onProceedTap()
        })
    }
}
