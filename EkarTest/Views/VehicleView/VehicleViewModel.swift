//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine

class VehicleViewModel: ObservableObject {
    
    @Published var isLoading: Bool = false
    @Published var vehicle: Vehicle = .empty
    
    @Published var showHowContractsWorkView = false
    
    @Published var selectedColor: String?
    @Published var showSelectedColorView = false
    
    private let vehicleProvider: IVehicleInfoProvider
    private let coordinator: IVehicleDetailsCoordinator
    
    private var subscriptions = Set<AnyCancellable>()
    
    init(
        vinCode: VinCode,
        vehicleProvider: IVehicleInfoProvider,
        coordinator: IVehicleDetailsCoordinator
    ) {
        self.vehicleProvider = vehicleProvider
        self.coordinator = coordinator
        
        getVehicleInfo(vinCode)
        
        $vehicle
            .dropFirst()
            .map { _ in false }
            .assign(to: &$isLoading)
    }
    
    private func getVehicleInfo(_ vinCode: VinCode) {
        isLoading = true
        vehicleProvider.getVehicleInfo(vinCode: vinCode)
            .receive(on: RunLoop.main)
            .catch { error -> Just<Vehicle> in
                // TODO: - show error to the users
                print(#function, "error", error.localizedDescription)
                return Just(Vehicle.empty)
            }
            .assign(to: &$vehicle)
    }
    
    func onProceedTap() {
        coordinator.onProceedTap()
    }
    
    func onColorSelect(_ color: String) {
        selectedColor = color
        showSelectedColorView = true
    }
    
    func onHowContractsWorkPressed() {
        showHowContractsWorkView = true
    }
}
