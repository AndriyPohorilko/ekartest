//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation

enum API {
    static let base = "https://api.carsxe.com/"
    static let specs = "specs"
    
    static let key = "tha91z6lv_j8u1nv4xs_ilfswb1e3"
}
