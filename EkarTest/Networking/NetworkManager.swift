//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine

struct NetworkManager {
    static func fetch<T: Decodable>(endPoint: String, params: [String: String]? = nil) -> AnyPublisher<T, APIError> {
        let urlString = "\(API.base)\(endPoint)"
        var urlComponents = URLComponents(string: urlString)
        
        if let params = params {
            urlComponents?.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
            let encodedQuery = urlComponents?.percentEncodedQuery
            urlComponents?.percentEncodedQuery = encodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        }
        
        guard let url = urlComponents?.url else {
            return Fail(outputType: T.self, failure: APIError.apiError("URLComponents is malformed, with base string: \(urlString)"))
                .eraseToAnyPublisher()
        }
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.waitsForConnectivity = true
        
        return URLSession(configuration: sessionConfig)
            .dataTaskPublisher(for: url)
            .tryMap {
                let decoder = JSONDecoder()
                
                return try decoder.decode(T.self, from: $0.data) }
            .mapError { APIError.apiError($0.localizedDescription) }
            .eraseToAnyPublisher()
    }
    
    static func readLocalJSON<T: Decodable>(fileName: String) -> AnyPublisher<T, APIError> {
        return Bundle.main.url(forResource: fileName, withExtension: "json")
            .publisher
            .tryMap { try Data(contentsOf: $0) }
            .tryMap {
                let decoder = JSONDecoder()
                return try decoder.decode(T.self, from: $0)
            }
            .mapError { APIError.apiError($0.localizedDescription) }
            .eraseToAnyPublisher()
    }
}
