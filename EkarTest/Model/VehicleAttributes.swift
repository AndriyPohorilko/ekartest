//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation

struct VehicleAttributes: Decodable {
    let year: String
    let make: String
    let model: String
    let trim: String
    let style: String
    let type: String
    let size: String
    let category: String
    let made_in: String
    let made_in_city: String
    let doors: String
    let fuel_type: String
    let fuel_capacity: String
    let city_mileage: String
    let highway_mileage: String
    let engine: String
    let engine_size: String
    let engine_cylinders: String
    let transmission: String
    let transmission_short: String
    let transmission_type: String
    let transmission_speeds: String
    let drivetrain: String
    let anti_brake_system: String
    let steering_type: String
    let curb_weight: String
    let gross_vehicle_weight_rating: String
    let overall_height: String
    let overall_length: String
    let overall_width: String
    let wheelbase_length: String
    let standard_seating: String
    let invoice_price: String
    let delivery_charges: String
    let manufacturer_suggested_retail_price: String
    let production_seq_number: String
    let front_brake_type: String
    let rear_brake_type: String
    let turning_diameter: String
    let front_suspension: String
    let rear_suspension: String
    let front_spring_type: String
    let rear_spring_type: String
    let tires: String
    let front_headroom: String
    let rear_headroom: String
    let front_legroom: String
    let rear_legroom: String
    let front_shoulder_room: String
    let rear_shoulder_room: String
    let front_hip_room: String
    let rear_hip_room: String
    let interior_trim: [String]
    let exterior_color: [String]
    let curb_weight_manual: String
    let ground_clearance: String
    let track_front: String
    let track_rear: String
    let cargo_length: String
    let width_at_wheelwell: String
    let width_at_wall: String
    let depth: String
    let optional_seating: String
    let passenger_volume: String
    let cargo_volume: String
    let standard_towing: String
    let maximum_towing: String
    let standard_payload: String
    let maximum_payload: String
    let maximum_gvwr: String
}
