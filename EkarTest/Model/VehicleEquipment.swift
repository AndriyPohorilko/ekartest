//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation

struct VehicleEquipment: Decodable {
    enum CodingKeys: String, CodingKey {
        case four_wd_awd = "4wd_awd"
        
        case abs_brakes = "abs_brakes"
        case adjustable_foot_pedals = "adjustable_foot_pedals"
        case air_conditioning = "air_conditioning"
        case alloy_wheels = "alloy_wheels"
        case am_fm_radio = "am_fm_radio"
        case automatic_headlights = "automatic_headlights"
        case automatic_load_leveling = "automatic_load_leveling"
        case cargo_area_cover = "cargo_area_cover"
        case cargo_area_tiedowns = "cargo_area_tiedowns"
        case cargo_net = "cargo_net"
        case cassette_player = "cassette_player"
        case cd_changer = "cd_changer"
        case cd_player = "cd_player"
        case child_safety_door_locks = "child_safety_door_locks"
        case chrome_wheels = "chrome_wheels"
        case cruise_control = "cruise_control"
        case daytime_running_lights = "daytime_running_lights"
        case deep_tinted_glass = "deep_tinted_glass"
        case driver_airbag = "driver_airbag"
        case driver_multi_adjustable_power_seat = "driver_multi_adjustable_power_seat"
        case dvd_player = "dvd_player"
        case electrochromic_exterior_rearview_mirror = "electrochromic_exterior_rearview_mirror"
        case electrochromic_interior_rearview_mirror = "electrochromic_interior_rearview_mirror"
        case electronic_brake_assistance = "electronic_brake_assistance"
        case electronic_parking_aid = "electronic_parking_aid"
        case first_aid_kit = "first_aid_kit"
        case fog_lights = "fog_lights"
        case front_air_dam = "front_air_dam"
        case front_cooled_seat = "front_cooled_seat"
        case front_heated_seat = "front_heated_seat"
        case front_power_lumbar_support = "front_power_lumbar_support"
        case front_power_memory_seat = "front_power_memory_seat"
        case front_side_airbag = "front_side_airbag"
        case front_side_airbag_with_head_protection = "front_side_airbag_with_head_protection"
        case front_split_bench_seat = "front_split_bench_seat"
        case full_size_spare_tire = "full_size_spare_tire"
        case genuine_wood_trim = "genuine_wood_trim"
        case glass_rear_window_on_convertible = "glass_rear_window_on_convertible"
        case heated_exterior_mirror = "heated_exterior_mirror"
        case heated_steering_wheel = "heated_steering_wheel"
        case high_intensity_discharge_headlights = "high_intensity_discharge_headlights"
        case interval_wipers = "interval_wipers"
        case keyless_entry = "keyless_entry"
        case leather_seat = "leather_seat"
        case leather_steering_wheel = "leather_steering_wheel"
        case limited_slip_differential = "limited_slip_differential"
        case load_bearing_exterior_rack = "load_bearing_exterior_rack"
        case locking_differential = "locking_differential"
        case locking_pickup_truck_tailgate = "locking_pickup_truck_tailgate"
        case manual_sunroof = "manual_sunroof"
        case navigation_aid = "navigation_aid"
        case passenger_airbag = "passenger_airbag"
        case passenger_multi_adjustable_power_seat = "passenger_multi_adjustable_power_seat"
        case pickup_truck_bed_liner = "pickup_truck_bed_liner"
        case pickup_truck_cargo_box_light = "pickup_truck_cargo_box_light"
        case power_adjustable_exterior_mirror = "power_adjustable_exterior_mirror"
        case power_door_locks = "power_door_locks"
        case power_sliding_side_van_door = "power_sliding_side_van_door"
        case power_sunroof = "power_sunroof"
        case power_trunk_lid = "power_trunk_lid"
        case power_windows = "power_windows"
        case rain_sensing_wipers = "rain_sensing_wipers"
        case rear_spoiler = "rear_spoiler"
        case rear_window_defogger = "rear_window_defogger"
        case rear_wiper = "rear_wiper"
        case remote_ignition = "remote_ignition"
        case removable_top = "removable_top"
        case run_flat_tires = "run_flat_tires"
        case running_boards = "running_boards"
        case second_row_folding_seat = "second_row_folding_seat"
        case second_row_heated_seat = "second_row_heated_seat"
        case second_row_multi_adjustable_power_seat = "second_row_multi_adjustable_power_seat"
        case second_row_removable_seat = "second_row_removable_seat"
        case second_row_side_airbag = "second_row_side_airbag"
        case second_row_side_airbag_with_head_protection = "second_row_side_airbag_with_head_protection"
        case second_row_sound_controls = "second_row_sound_controls"
        case separate_driver_front_passenger_climate_controls = "separate_driver_front_passenger_climate_controls"
        case side_head_curtain_airbag = "side_head_curtain_airbag"
        case skid_plate = "skid_plate"
        case sliding_rear_pickup_truck_window = "sliding_rear_pickup_truck_window"
        case splash_guards = "splash_guards"
        case steel_wheels = "steel_wheels"
        case steering_wheel_mounted_controls = "steering_wheel_mounted_controls"
        case subwoofer = "subwoofer"
        case tachometer = "tachometer"
        case telematics_system = "telematics_system"
        case telescopic_steering_column = "telescopic_steering_column"
        case third_row_removable_seat = "third_row_removable_seat"
        case tilt_steering = "tilt_steering"
        case tilt_steering_column = "tilt_steering_column"
        case tire_pressure_monitor = "tire_pressure_monitor"
        case tow_hitch_receiver = "tow_hitch_receiver"
        case towing_preparation_package = "towing_preparation_package"
        case traction_control = "traction_control"
        case trip_computer = "trip_computer"
        case trunk_anti_trap_device = "trunk_anti_trap_device"
        case vehicle_anti_theft = "vehicle_anti_theft"
        case vehicle_stability_control_system = "vehicle_stability_control_system"
        case voice_activated_telephone = "voice_activated_telephone"
        case wind_deflector_for_convertibles = "wind_deflector_for_convertibles"
    }
    
    let four_wd_awd: String
    let abs_brakes: String
    let adjustable_foot_pedals: String
    let air_conditioning: String
    let alloy_wheels: String
    let am_fm_radio: String
    let automatic_headlights: String
    let automatic_load_leveling: String
    let cargo_area_cover: String
    let cargo_area_tiedowns: String
    let cargo_net: String
    let cassette_player: String
    let cd_changer: String
    let cd_player: String
    let child_safety_door_locks: String
    let chrome_wheels: String
    let cruise_control: String
    let daytime_running_lights: String
    let deep_tinted_glass: String
    let driver_airbag: String
    let driver_multi_adjustable_power_seat: String
    let dvd_player: String
    let electrochromic_exterior_rearview_mirror: String
    let electrochromic_interior_rearview_mirror: String
    let electronic_brake_assistance: String
    let electronic_parking_aid: String
    let first_aid_kit: String
    let fog_lights: String
    let front_air_dam: String
    let front_cooled_seat: String
    let front_heated_seat: String
    let front_power_lumbar_support: String
    let front_power_memory_seat: String
    let front_side_airbag: String
    let front_side_airbag_with_head_protection: String
    let front_split_bench_seat: String
    let full_size_spare_tire: String
    let genuine_wood_trim: String
    let glass_rear_window_on_convertible: String
    let heated_exterior_mirror: String
    let heated_steering_wheel: String
    let high_intensity_discharge_headlights: String
    let interval_wipers: String
    let keyless_entry: String
    let leather_seat: String
    let leather_steering_wheel: String
    let limited_slip_differential: String
    let load_bearing_exterior_rack: String
    let locking_differential: String
    let locking_pickup_truck_tailgate: String
    let manual_sunroof: String
    let navigation_aid: String
    let passenger_airbag: String
    let passenger_multi_adjustable_power_seat: String
    let pickup_truck_bed_liner: String
    let pickup_truck_cargo_box_light: String
    let power_adjustable_exterior_mirror: String
    let power_door_locks: String
    let power_sliding_side_van_door: String
    let power_sunroof: String
    let power_trunk_lid: String
    let power_windows: String
    let rain_sensing_wipers: String
    let rear_spoiler: String
    let rear_window_defogger: String
    let rear_wiper: String
    let remote_ignition: String
    let removable_top: String
    let run_flat_tires: String
    let running_boards: String
    let second_row_folding_seat: String
    let second_row_heated_seat: String
    let second_row_multi_adjustable_power_seat: String
    let second_row_removable_seat: String
    let second_row_side_airbag: String
    let second_row_side_airbag_with_head_protection: String
    let second_row_sound_controls: String
    let separate_driver_front_passenger_climate_controls: String
    let side_head_curtain_airbag: String
    let skid_plate: String
    let sliding_rear_pickup_truck_window: String
    let splash_guards: String
    let steel_wheels: String
    let steering_wheel_mounted_controls: String
    let subwoofer: String
    let tachometer: String
    let telematics_system: String
    let telescopic_steering_column: String
    let third_row_removable_seat: String
    let tilt_steering: String
    let tilt_steering_column: String
    let tire_pressure_monitor: String
    let tow_hitch_receiver: String
    let towing_preparation_package: String
    let traction_control: String
    let trip_computer: String
    let trunk_anti_trap_device: String
    let vehicle_anti_theft: String
    let vehicle_stability_control_system: String
    let voice_activated_telephone: String
    let wind_deflector_for_convertibles: String
}

extension VehicleEquipment {
    func toEquipmentItems() -> [EquipmentItem] {
        var items = [EquipmentItem]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                let key = key.replacingOccurrences(of: "_", with: " ").capitalized
                if let value = child.value as? String {
                    items.append(EquipmentItem(name: key, value: value))
                }
            }
        }
        return items
    }
}

struct EquipmentItem: Identifiable {
    var id: String {
        return name
    }
    
    let name: String
    let value: String
}

enum VehicleInfoItemType: String {
    case engine, seats, gearbox, fuelType
    
    var icon: String {
        switch self {
        case .engine:
            return "car"
        case .seats:
            return "figure.stand"
        case .gearbox:
            return "gearshape.2.fill"
        case .fuelType:
            return "bookmark"
        }
    }
}

struct VehicleInfoItem: Identifiable {
    var id: String {
        return type.rawValue
    }
    
    let type: VehicleInfoItemType
    let value: String
}
