//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation

struct Price {
    let value: String
    let currency: String
}

struct Vehicle {
    let make: String
    let model: String
    let style: String
    let iconName: String
    let year: String
    let price: Price
    let colors: [String]
    let infoItems: [VehicleInfoItem]
    let equipmentItems: [EquipmentItem]
    
    init(dto: VehicleDTO) {
        self.make = dto.attributes.make
        self.model = dto.attributes.model
        self.style = dto.attributes.style
        self.iconName = "\(dto.attributes.make)_\(dto.attributes.model)"
        self.year = dto.attributes.year
        self.price = Vehicle.getPrice(dto.attributes.invoice_price)
        self.colors = dto.attributes.exterior_color
        
        self.infoItems = [
            VehicleInfoItem(type: .engine, value: dto.attributes.engine),
            VehicleInfoItem(type: .seats, value: dto.attributes.standard_seating + "Seats"),
            VehicleInfoItem(type: .gearbox, value: dto.attributes.transmission_short),
            VehicleInfoItem(type: .fuelType, value: dto.attributes.fuel_type.isEmpty ? "Petrol" : dto.attributes.fuel_type),
        ]
        self.equipmentItems = dto.equipment.toEquipmentItems()
    }
    
    init(
        make: String = "",
        model: String = "",
        style: String = "",
        iconName: String = "",
        year: String = "",
        price: Price = Price(value: "", currency: ""),
        colors: [String] = [],
        infoItems: [VehicleInfoItem] = [],
        equipmentItems: [EquipmentItem] = []
    ) {
        self.make = make
        self.model = model
        self.style = style
        self.iconName = iconName
        self.year = year
        self.price = price
        self.colors = colors
        self.infoItems = infoItems
        self.equipmentItems = equipmentItems
    }
    
    static let empty = Vehicle()
    
    private static func getPrice(_ price: String) -> Price {
        let priceComponents = price.split(separator: " ")
        
        return Price(
            value: String(priceComponents.first ?? ""),
            currency: String(priceComponents.last ?? "")
        )
    }
}
