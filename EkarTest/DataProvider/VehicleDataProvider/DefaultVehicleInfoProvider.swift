//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine

class DefaultVehicleInfoProvider: IVehicleInfoProvider {
    func getVehicleInfo(vinCode: VinCode) -> AnyPublisher<Vehicle, APIError> {
        let params = [
            "key": API.key,
            "vin": vinCode.code
        ]
        
        return NetworkManager.fetch(endPoint: API.specs, params: params)
            .map {
                Vehicle(dto: $0)
            }
            .eraseToAnyPublisher()
    }
}

class TestVehicleDataProvider: IVehicleInfoProvider {
    func getVehicleInfo(vinCode: VinCode) -> AnyPublisher<Vehicle, APIError> {
        NetworkManager.readLocalJSON(fileName: "TestVehicle")
            .map {
                Vehicle(dto: $0)
            }
            .eraseToAnyPublisher()
    }
}
