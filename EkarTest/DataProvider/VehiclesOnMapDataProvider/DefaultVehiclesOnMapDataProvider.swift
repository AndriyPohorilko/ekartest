//
// Copyright Andriy Pohorilko©. 2016-2021. All rights reserved.
//
// andriy.pohorilko@gmail.com
// pohorilkodev.com
//
// This source code is protected by international copyright law. Unauthorized
// reproduction, reverse-engineering, or distribution of all or any portion of
// this source code is strictly prohibited.
// 

import Foundation
import Combine
import CoreLocation

class DefaultVehiclesOnMapDataProvider: IVehiclesOnMapDataProvider {
    func getVehiclesAroundLocation(location: CLLocationCoordinate2D) -> AnyPublisher<[VehicleOnMap], APIError> {
        // since I don't have API to get list of all available vehicles near user current location I create few identical vehicles with different random locations around
        
        var vehicles = [VehicleOnMap]()
        for _ in 0..<10 {
            vehicles.append(getVehicleWithRandomLocation(near: location))
        }
        
        return Just(vehicles)
            .setFailureType(to: APIError.self)
            .eraseToAnyPublisher()
    }
    
    private func getVehicleWithRandomLocation(near location: CLLocationCoordinate2D) -> VehicleOnMap {
        return VehicleOnMap(
            vin: VinCode(code: "JTDZN3EU0E3298500"),
            coordinate: getRandomLocation(near: location)
        )
    }
    
    private func getRandomLocation(near location: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let randomDouble = Double.random(in: 0.01...0.05)
        let latitude = Bool.random() ? location.latitude + randomDouble : location.latitude - randomDouble
        let longitude = Bool.random() ? location.longitude + randomDouble : location.longitude - randomDouble
        
        return CLLocationCoordinate2D(
            latitude: CLLocationDegrees(latitude),
            longitude: CLLocationDegrees(longitude)
        )
    }
}
