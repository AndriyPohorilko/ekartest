#  EkarTest

This project is created as a test task.

It requires accessing device location, so please allow it on the  app start. 

If you run it on the simulator, make sure you have turned on **"Simulate location"**, otherwise you won't see any available cars around your location.

Please do not hesitate to contact me if I can be of any further assistance / provide further information on this matter.

*andriy.pohorilko@gmail.com*

*+380977581254*
